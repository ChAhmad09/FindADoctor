package com.muhammadahmadcostomgridview.customgridview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class DisplayActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display);
        String username = getIntent().getStringExtra("UserName");

        TextView tv = (TextView) findViewById(R.id.TvFirstname);
        tv.setText(username);
    }
    public void onshopClick(View v) {
        if (v.getId()==R.id.btnbook){
            Intent i = new Intent(DisplayActivity.this, MainActivity.class);

            startActivity(i);
        }
    }
}
