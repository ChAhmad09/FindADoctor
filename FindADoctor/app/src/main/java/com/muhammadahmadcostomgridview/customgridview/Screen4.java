package com.muhammadahmadcostomgridview.customgridview;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;


public class Screen4 extends AppCompatActivity {

    private ListView listView;

    public static String [] prgmNameList = {
            "Dr. Ausaf Ahmed" ,
            "Dr. Zulkifal Awan",
            "Dr. Arfat Jawaid",
            "Dr. Muhammad Saleem Marfani",
            "Dr. Muntsir",
            "Dr. Malik Msood Ahmed",
            "Dr. Javed Hayat",
            "Dr. Istiaq Rasool",
            "Dr. Syed Nadeem Hussain Rizvi",
            "Dr. Kazi T Adil",
            "Dr. Nadeem Qamar",
            "Dr. Nawaz Lashari",
            "Dr. Tariq Masood",
            "Dr. M. Anwar Jalil",
            "Dr. Irfan Bakhsi"
    };

    public static String [] prgmDescriptionList = {
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist",
            "ENT Specialist"

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);

        listView = (ListView) findViewById(R.id.lstvCompPrograms);
        listView.setAdapter(new fourthAdpter(Screen4.this, prgmNameList,prgmDescriptionList));
    }


}