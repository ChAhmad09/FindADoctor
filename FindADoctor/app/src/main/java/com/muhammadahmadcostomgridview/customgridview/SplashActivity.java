package com.muhammadahmadcostomgridview.customgridview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        Thread a= new Thread(){
            @Override
            public void run() {
                try {
                    sleep(2500);
                    Intent b=new Intent(getApplicationContext(), LoginActivity.class);
                    startActivity(b);
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        a.start();
    }
}
