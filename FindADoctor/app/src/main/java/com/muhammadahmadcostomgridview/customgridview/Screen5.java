package com.muhammadahmadcostomgridview.customgridview;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;


public class Screen5 extends AppCompatActivity {

    private ListView listView;

    public static String [] prgmNameList = {
            "Dr. Nasir Sulaiman" ,
            "Dr. Fateh Khan Akhtar",
            "Dr. Mumtaz Ahmad",
            "Dr. Kamran K. Cheema",
            "Prof. Khalid Javed Rabbani",
            "Dr. Munir Amin Mughal",
            "Dr. Khalid Rabbani",
            "Dr. Munir Amin Mughal",
            "Dr. Nasir Sulaiman",
            "Dr. Amanullah Memon",
            "Dr. Capt.(R) Moeen A. Qureshi",
            "Dr. Farhat Abbas",
            "Dr. Raziuddin Biyabani",
            "Dr. Syed Saeed Abidi",
            "Dr. Muhammad Shoaib Mithani"
    };

    public static String [] prgmDescriptionList = {
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist",
            "Urologist"

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen5);

        listView = (ListView) findViewById(R.id.lstvCompPrograms);
        listView.setAdapter(new FifthAdapter(Screen5.this, prgmNameList,prgmDescriptionList));
    }


}