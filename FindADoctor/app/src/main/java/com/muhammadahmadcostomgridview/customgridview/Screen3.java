package com.muhammadahmadcostomgridview.customgridview;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;


public class Screen3 extends AppCompatActivity {

    private ListView listView;

    public static String [] prgmNameList = {
                    "Dr. Muhammad Javed",
                    "Dr. Amer Awan",
                    "Dr. Badar Javed",
                    "Dr. Tariq Saeed",
                    "Dr. Khalid Mahmood",
                    "Dr. Edgar Gulzar",
                    "Dr. Wajahat Latif",
                    "Dr. S.M. Tahir",
                    "Dr. M. H. Ejaz Sandhu",
                    "Dr. Akram Riaz",
                    "Dr. Muhammad Tayyab",
                    "Dr. M. alim Akhtar",
                    "Dr. Shehbaz Saeed Sheikh",
            "Dr. Muhammad Shahbaz",
            "Dr. Zahid Chaudhry"
    };

    public static String [] prgmDescriptionList = {
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist",
            "Eye Specialist"

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sreen3);

        listView = (ListView) findViewById(R.id.lstvCompPrograms);
        listView.setAdapter(new threeAdapter(Screen3.this, prgmNameList,prgmDescriptionList));
    }


}