package com.muhammadahmadcostomgridview.customgridview;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;


public class Screen9 extends AppCompatActivity {

    private ListView listView;

    public static String [] prgmNameList = {
            "Dr. Wajahat Latif",
            "Dr. S.M. Tahir",
            "Dr. M. H. Ejaz Sandhu",
            "Dr. Akram Riaz",
            "Dr. Muhammad Tayyab",
            "Dr. M. alim Akhtar",
            "Dr. Shehbaz Saeed Sheikh",
            "Dr. Kazi T Adil",
            "Dr. Nadeem Qamar",
            "Dr. Nawaz Lashari",
            "Dr. Tariq Masood",
            "Dr. M. Anwar Jalil",
            "Dr. Irfan Bakhsi"
    };

    public static String [] prgmDescriptionList = {
            "Oncologist",
            "Oncologist",
           "Oncologist",
            "Oncologist",
            "Oncologist",
            "Oncologist",
            "Oncologist",
            "Oncologist",
            "Oncologist",
            "Oncologist",
            "Oncologist",
            "Oncologist",
            "Oncologist"

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen9);

        listView = (ListView) findViewById(R.id.lstvCompPrograms);
        listView.setAdapter(new NinthAdapter(Screen9.this, prgmNameList,prgmDescriptionList));
    }


}