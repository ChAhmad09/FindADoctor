package com.muhammadahmadcostomgridview.customgridview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class SignUpActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
    }
    public void onSignClick(View v){
        if (v.getId()==R.id.btnsign){

            EditText fname=(EditText)findViewById(R.id.TvFirstname);
            String fnames=fname.getText().toString();

            EditText lname=(EditText)findViewById(R.id.TvLname);
            String lnmaes=lname.getText().toString();

            EditText email=(EditText)findViewById(R.id.Tvemail);
            String emails=email.getText().toString();

            EditText pass=(EditText)findViewById(R.id.Tvpassword);
            String passs=pass.getText().toString();

            EditText pass1=(EditText)findViewById(R.id.TvcPassword);
            String pass1s=pass1.getText().toString();

            if (fname.length()>0&&lname.length()>0&&email.length()>0&&pass.length()>0&&pass1.length() >0) {

                if (!passs.equals(pass1s)) {
                    //popup msg
                    Toast.makeText(SignUpActivity.this, "Password Dosn't Match!", Toast.LENGTH_SHORT).show();
                }
                else {
                    Toast.makeText(SignUpActivity.this, "Successfully:You Are Register! ", Toast.LENGTH_SHORT).show();
                }
            }else {
                Toast.makeText(SignUpActivity.this,"Plz Enter Your Full & Correct Dtails!",Toast.LENGTH_SHORT).show();
            }

        }

    }
}
