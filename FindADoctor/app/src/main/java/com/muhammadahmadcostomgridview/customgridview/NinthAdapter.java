package com.muhammadahmadcostomgridview.customgridview;

/**
 * Created by MUHAMMAD AHMAD on 16-Jul-16.
 */

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;



public class NinthAdapter extends BaseAdapter {

    private Context context;
    private static LayoutInflater inflater = null;
    String [] progNames;
    String [] prgmDescriptions;
    public NinthAdapter(Screen9 mainActivity, String[] prgmNameList, String[] descriptions) {
        // TODO Auto-generated constructor stub
        progNames = prgmNameList;
        prgmDescriptions = descriptions;
        context = mainActivity;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return progNames.length;
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    public class Holder
    {
        TextView tv;
        TextView txtvDescription;
        ImageView img;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {
        Holder holder = new Holder();
        View rowView;
        rowView = inflater.inflate(R.layout.row_program_list, null);
        holder.tv = (TextView) rowView.findViewById(R.id.txtComputerLangName);
        holder.txtvDescription = (TextView) rowView.findViewById(R.id.txtDescription);


        holder.tv.setText(progNames[i]);
        holder.txtvDescription.setText(prgmDescriptions[i]);
        rowView.setOnClickListener(new View.OnClickListener() {



            @Override
            public void onClick(View view) {
                Toast.makeText(context, "You Clicked "+ progNames[i], Toast.LENGTH_LONG).show();


            }
        });
        return rowView;
    }
}

