package com.muhammadahmadcostomgridview.customgridview;

import java.util.ArrayList;
import android.content.Intent;
import android.os.Bundle;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.ActionBarActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.Toast;

import com.muhammadahmadcostomgridview.customgridview.R;


public class MainActivity extends ActionBarActivity {
    private GridView gridView;
    private GridViewAdapter gridAdapter;
    String s;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        gridView = (GridView) findViewById(R.id.gridView);
        gridAdapter = new GridViewAdapter(this, R.layout.grid_item, getData());
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener(new OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {

                ImageItem item = (ImageItem) parent.getItemAtPosition(position);

                Toast.makeText(MainActivity.this, "" + position, Toast.LENGTH_SHORT).show();
                Intent myIntent = null;
                if (position == 0) {
                    myIntent = new Intent(v.getContext(), Screen1.class);
                    myIntent.putExtra("UserName", s);

                }
                if (position == 1) {
                    myIntent = new Intent(v.getContext(), Screen2.class);
                }
                if (position == 2) {
                    myIntent = new Intent(v.getContext(), Screen3.class);
                }
                startActivity(myIntent);

            }
        });

    }
    String[] countries = new String[]{"Pakistan", "Iran", "China"};
    /**
     * Prepare some dummy data for gridview
     */
    private ArrayList<ImageItem> getData() {
        final ArrayList<ImageItem> imageItems = new ArrayList<>();
        TypedArray imgs = getResources().obtainTypedArray(R.array.image_ids);

        for (int i = 0; i < imgs.length(); i++) {
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), imgs.getResourceId(i, -1));

            imageItems.add(new ImageItem(bitmap,""));
        }
        return imageItems;


    }

}
