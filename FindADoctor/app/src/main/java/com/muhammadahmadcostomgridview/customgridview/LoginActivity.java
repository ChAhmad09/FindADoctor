package com.muhammadahmadcostomgridview.customgridview;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {
    String pa;
    String s;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    public void onButtonClick(View v) {

        if (v.getId() == R.id.btnlogin) {
            EditText a = (EditText) findViewById(R.id.Etname);
            String s = a.getText().toString();
            EditText b = (EditText) findViewById(R.id.Etpassword);
            String pa = b.getText().toString();

            if (s.length() > 0 && pa.length() > 0) {
                Intent i = new Intent(LoginActivity.this, DisplayActivity.class);
                i.putExtra("UserName", s);
                startActivity(i);
            } else {
                Toast err = Toast.makeText(LoginActivity.this, "Plz Enter Your UserName", Toast.LENGTH_SHORT);
                err.show();
                ;
            }

        }
            if (v.getId() == R.id.btnsignup) {
                Intent j = new Intent(LoginActivity.this, SignUpActivity.class);

                startActivity(j);
            }

        }
    }



