package com.muhammadahmadcostomgridview.customgridview;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;


public class Screen7 extends AppCompatActivity {

    private ListView listView;

    public static String [] prgmNameList = {
                    "Dr. Qasim Saleem" ,
                    "Dr. Malik Mohammad Ali",
                    "Dr. Muhammad Asad Mirza",
                    "Dr. Muhammad Sami Ullah",
                    "Dr. Nauman Nasir",
                    "Dr. Rizwan Memon",
                    "Dr. Kabir Ahmad",
                    "Dr. Moeen Ud Din Bhatty",
                    "Dr. Faheem Ullah",
                    "Dr. Fida Hussain",
                    "Dr. Shehla Shah",
            "Dr. Nawaz Lashari",
            "Dr. Tariq Masood",
            "Dr. M. Anwar Jalil",
            "Dr. Irfan Bakhsi"
    };

    public static String [] prgmDescriptionList = {
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist",
            "Dentist"

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen7);

        listView = (ListView) findViewById(R.id.lstvCompPrograms);
        listView.setAdapter(new SevenAdapter(Screen7.this, prgmNameList,prgmDescriptionList));
    }


}