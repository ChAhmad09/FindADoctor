package com.muhammadahmadcostomgridview.customgridview;


import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;


public class Screen1 extends AppCompatActivity {

    private ListView listView;

    public static String [] prgmNameList = {
            "Dr. Liaquat Ali Cheema" ,
            "Dr. Abdul Hafeez Khan",
            "Dr. Habib Ur Rahman",
            "Dr. Abdul Wahab Yousafzai",
            "Dr. Rizwan Taj",
            "Dr. Waqas Ahmed",
            "Dr. Javed Hayat",
            "Dr. Istiaq Rasool",
            "Dr. Syed Nadeem Hussain Rizvi",
            "Dr. Kazi T Adil",
            "Dr. Nadeem Qamar",
            "Dr. Nawaz Lashari",
            "Dr. Tariq Masood",
            "Dr. M. Anwar Jalil",
            "Dr. Irfan Bakhsi"
    };

    public static String [] prgmDescriptionList = {
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist",
            "Heart Specialist"

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen1);

        listView = (ListView) findViewById(R.id.lstvCompPrograms);
        listView.setAdapter(new customadapter(Screen1.this, prgmNameList,prgmDescriptionList));
    }


}