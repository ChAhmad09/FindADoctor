package com.muhammadahmadcostomgridview.customgridview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ListView;


public class Screen2 extends AppCompatActivity {

    private ListView listView;

    public static String [] prgmNameList = {
                           "Dr. Mughis Sheerani" ,
                    "Dr. Nadir Ali Syed",
                    "Dr. Muhammad Akmal Hussain (Ms)",
                    "Dr. Nadir Ali Syed",
                    "Dr. Muhammad Shahid Mustafa",
            "Dr. Ayeesha Kamran Kamal",
            "Dr. Javed Hayat",
            "Dr. Istiaq Rasool",
            "Dr. Syed Nadeem Hussain Rizvi",
            "Dr. Kazi T Adil",
            "Dr. Nadeem Qamar",
            "Dr. Nawaz Lashari",
            "Dr. Tariq Masood",
            "Dr. M. Anwar Jalil",
            "Dr. Irfan Bakhsi"
    };

    public static String [] prgmDescriptionList = {
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist",
            "Neurologist"

    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen2);

        listView = (ListView) findViewById(R.id.lstvCompPrograms);
        listView.setAdapter(new ScreentoAdapter(Screen2.this, prgmNameList,prgmDescriptionList));
    }


}
